﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{

    public Transform tf;
    public float moveSpeed;
    public float lifetime;

    // Use this for initialization
    void Start() {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update() {
        Move();
        DestroyObjectDelayed();
    }

    public void Move() { // Moves the bullets
        tf.position += tf.up * moveSpeed;
    }

    public void DestroyObjectDelayed() { // Destroys the bullet GameObject after a certain amount of time
        Destroy(this.gameObject, lifetime);
    }

    public void OnTriggerEnter2D(Collider2D otherCollider) { // Only destroys GameObjects with the tag of "Enemy."
        if (otherCollider.tag == "Enemy")
        {
            Destroy(otherCollider.gameObject);
            Destroy(gameObject);
        }
    }
}
