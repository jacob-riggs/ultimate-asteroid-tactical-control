﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playableArea : MonoBehaviour {

    public void OnTriggerExit2D(Collider2D playerShip) {
        // If they leave the playable area, the player dies
        Destroy(playerShip.gameObject);
    }
}
