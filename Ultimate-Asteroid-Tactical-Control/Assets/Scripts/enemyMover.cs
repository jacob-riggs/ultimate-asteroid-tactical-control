﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMover : MonoBehaviour {

    private Transform tf;
    public Transform targetTf;
    public float moveSpeed;
    public Rigidbody2D rg;
    public bool isAlwaysSeeking;
    private Vector3 movementVector;
    public bool isDirectional;

    // Use this for initialization
    void Start()
    {
        targetTf = GameObject.FindWithTag("Player").transform; //Sets the target as an object with the tag "Player," which is the playerShip
        tf = GetComponent<Transform>();
        rg = GetComponent<Rigidbody2D>();
        // Target the player
        movementVector = targetTf.position - tf.position;

    }

    // Update is called once per frame
    void Update()
    {
        if (isAlwaysSeeking) {
            movementVector = targetTf.position - tf.position; // end position - start position
        }

        // Move on each framedraw
        movementVector.Normalize(); // make it a length of one
        movementVector = movementVector * moveSpeed; // make it a length of speed
        tf.position = tf.position + movementVector;

        // if we need to look at the player
        if (isDirectional) {
            tf.right = movementVector;
        }
    }
}
