﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathZone : MonoBehaviour {

    public void OnTriggerEnter2D(Collider2D otherCollider){

        if (otherCollider.tag == "Player"){ // If a player touches the deathzone, they die.
            Destroy(otherCollider.gameObject);
        }
    }

}
