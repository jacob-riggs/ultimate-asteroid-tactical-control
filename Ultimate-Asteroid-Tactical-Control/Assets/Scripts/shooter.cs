﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooter : MonoBehaviour {

    public GameObject bulletPrefab;
    public Transform shootPoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) { // Will shoot when space is pressed
            Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
        }
	}
}
