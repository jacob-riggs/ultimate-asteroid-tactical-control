﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour {

    public GameObject playerShipPrefab; // GameObject for the playerShip to be spawned
    public GameObject playerShip;
    public static List<GameObject> enemyChoices = new List<GameObject>(); // List for the enemies
    public int numEnemies = 0; // Sets the starting value of how many enemies there are to 0
    private List<GameObject> enemies = new List<GameObject>();
    public int maxEnemies = 3; // Sets the maximum amount of enemies
    Vector3 playerSpawn = new Vector3(0, 0, 0); // Position for the player to spawn
    public int Lives; // Variable that will hold the number of lives
    public Texture tex; // Variable for lives texture
    private float texWidth; // Will get the texture's width
    private float texHeight; // Will get the texture's height

    // Use this for initialization
    void Start() {
        Instantiate(playerShipPrefab, playerSpawn, Quaternion.identity); // Instantiates the player to spawn in the center of the map
        Object[] subListObjects = Resources.LoadAll("Prefabs", typeof(GameObject)); // Puts all enemyPrefabs into a list
        foreach (GameObject subListObject in subListObjects)
        {
            GameObject lo = (GameObject)subListObject;
            enemyChoices.Add(lo);
        }
        texWidth = tex.width;
        texHeight = tex.height;
    }

    void spawnEnemy() {
        // Spawns an enemy in a position from 0 to 6 (adjust for any prefabs for enemies added)
        int whichItem = Random.Range(0, 6);
        GameObject myObj = Instantiate(enemyChoices[whichItem]) as GameObject;
        enemies.Add(myObj);
        myObj.transform.position = transform.position;
    }

    // Update is called once per frame
    void Update () {

        if (enemies.Count < maxEnemies) {
            // Where the enemy will spawn
            transform.position = (Random.insideUnitCircle * 3) * 10;

            spawnEnemy();
        }

        isPlayerAlive();

	}
    private void LateUpdate() {
        // Remove destroyed objects
        enemies.RemoveAll(o => (o == null || o.Equals(null)));
    }

    void isPlayerAlive() {
        if (Lives != 0) { // Checks to see if the player still has lives.
            if (GameObject.FindGameObjectWithTag("Player") == null) {
                Lives--;
                foreach(GameObject GameObject in enemies) { // Goes through enemies and destroys each GameObject
                    Destroy(GameObject);
                }
                Instantiate(playerShipPrefab, playerSpawn, Quaternion.identity); // Instantiates the player to spawn in the center of the map again if they die
            }
        }
        else { // Closes the application of the player has no lives left
            Application.Quit();
        }
    }

    private void OnGUI() { // Displays a ship in the corner that disappears as the lives disappear.
        if (Lives > 0) {
            Rect posRect = new Rect(50, 50, texWidth / 3 * Lives, texHeight);
            Rect texRect = new Rect(0, 0, 1.0f / 3 * Lives, 1.0f);
            GUI.DrawTextureWithTexCoords(posRect, tex, texRect);
        }
    }
}
